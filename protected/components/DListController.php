<?php
/**
 * Список
 * Родительский класс для отображения списочных данных типа новостей, статей,
 * списка вакансий и др. данных, которые нужно отобразить списком с подробным
 * просмотром. Без иерархии.
 * @author alx
 *
 */
class DListController extends Controller
{
	/**
	 * ActiveRecord-модель
	 * ActiveRecord-модель, данные которой будеут выводится контроллером
	 * @var string
	 */
	public $contentModel = '';
	
	/**
	 * Шаблон для отображения списка записей
	 * @var string
	 */
	public $listTemplate = 'list';
	
	/**
	 * Шаблон для отображения одной записи
	 * @var string
	 */
	public $recordTemplate = 'record';
	
	public function init()
	{
		parent::init();
		
		if ( empty($this->contentModel))
		{
			throw new ErrorException('Не указано свойство contentModel для потомка класса '.__CLASS__);
		}
		if ( empty($this->listTemplate) )
		{
			throw new ErrorException('Не указан шаблон listTemplate для потомка класса '.__CLASS__);
		}
		if ( empty($this->recordTemplate) )
		{
			throw new ErrorException('Не указан шаблон recordTemplate для потомка класса '.__CLASS__);
		}
		  
	}
	
	public function actionIndex ()
	{	
		$staticModel = call_user_func(array($this->contentModel,'model'));
		$list = $staticModel->active()->notRemoved()->findAll();
		
		$this->render($this->listTemplate,array('list' => $list));
	}
	
	public function actionID()
	{	
		$id = (int) Yii::app()->request->getParam('id');

		$staticModel = call_user_func(array($this->contentModel,'model'));
		
		$record = $staticModel->active()->notRemoved()->findByPk($id);
		
		$this->render($this->recordTemplate, array('record' => $record));		
	}
}