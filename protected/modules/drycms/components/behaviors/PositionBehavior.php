<?php
class PositionBehavior extends CActiveRecordBehavior
{
	public $field_position = 'position';
	
	public function sorted($direction = 'ASC') {
		$this->getOwner()->dbCriteria->order = 't.'.$this->field_position.' '.$direction;
		return $this->getOwner();
	}
}