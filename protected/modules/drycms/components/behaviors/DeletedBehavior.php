<?php
class DeletedBehavior extends CActiveRecordBehavior
{
	public $field_deleted = 'deleted';
	
	public function remove()
	{
		$this->getOwner()->{$this->field_deleted} = 1;
		return $this->getOwner();
	}
	
	public function restore()
	{
		$this->getOwner()->{$this->field_deleted} = 0;
		return $this->getOwner();
	}
	
	public function notRemoved()
	{
		$criteria = $this->getOwner()->dbCriteria;
		$criteria->compare($this->field_deleted, 0);
		return $this->getOwner();
	}
	
	public function removed()
	{
		$criteria = $this->getOwner()->dbCriteria;
		$criteria->compare($this->field_deleted, 1);
		return $this->getOwner();
	}
	
	public function isRemoved()
	{
		return (boolean) $this->getOwner()->{$this->field_deleted};
	}
}