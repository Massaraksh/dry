<?php
class TreeBehavior extends CActiveRecordBehavior
{
	public $field_parent = 'parent_id';
	
	protected $_parents;
	
	public function root()
	{
		$this->getOwner()->dbCriteria->compare($this->field_parent, 0);
		return $this->getOwner();
	}
	
	public function childOf($id)
	{
		$this->getOwner()->dbCriteria->compare($this->field_parent, $id);
		return $this->getOwner();
	}
	
	public function getParents()
	{
		
		if ($this->_parents === NULL) {
			$this->_parents = array();
			$parent = $this->getOwner()->parent;
			while ($parent)
			{
				$this->_parents[] = $parent;
				$parent = $parent->parent;
			}
		}
		
		return $this->_parents;
	}
}