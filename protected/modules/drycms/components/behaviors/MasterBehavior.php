<?php
class MasterBehavior extends CActiveRecordBehavior
{
	public $structure;
	public $section;
	protected $_masterColumn;

	public function getMasterColumn()
	{
		if (!$this->_masterColumn && isset($this->structure[$this->section]['master'])) {
			$masterSection = $this->structure[$this->section]['master']['sectionId']; 
			$masterModelName = $this->structure[$masterSection]['model'];
			$masterModel = new $masterModelName();
			$masterRalations = $masterModel->relations();
			$this->_masterColumn = $masterRalations[$this->structure[$this->section]['master']['relation']][2];
		}
		
		return $this->_masterColumn;
	}
	
	public function master($masterId)
	{
		if ($this->masterColumn) {
			$this->getOwner()->dbCriteria->compare($this->masterColumn, $masterId);
		}
		
		return $this->getOwner();
	}
}