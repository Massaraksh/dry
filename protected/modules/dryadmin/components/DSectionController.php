<?php
class DSectionController extends CController
{
	
	public $layout = 'twocolumn';
	
	/**
	 * Название ActiveRecord-модели 
	 * @var string
	 */
	protected $_model = NULL;
	
	protected $_breadcrumbs = NULL;

        /**
	 * Информация о разделе админки
	 * @var DSection
	 */
	public $section;
        
        protected $recset=array();  //---V.Shirokov
        protected $nlevel;
	
	public function init()
	{
		$this->section = new DSection($this->actionParams['section'], $this);
		return parent::init();
	}

//---V.Shirokov        
        public function buildTree($records)
        {
        foreach($records as $rec)
           {
           $this->recset[]=$rec;
           $this->recset[count($this->recset)-1]->level=$this->nlevel;
           if($rec->children){$this->nlevel++;$this->buildTree($rec->children);}
           }
        $this->nlevel--;   
        }        
        
	/**
	 * Список записей
	 * @param unknown_type $parent
	 */
	public function actionIndex($section = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		if (isset($modelBehaviors['deleted'])){
			// Непоказываем удаленные
			$model->notRemoved();
		}
		
		if (isset($modelBehaviors['position'])){
			// Сортируем
			$model->sorted();
		}
                
		if (!empty($master)) {
			$model->dbCriteria->compare($this->section->foreignColumn,$master);
		}
		
		$records = $model->findAll();

		if (isset($modelBehaviors['tree'])){
                $this->nlevel=0;
                $records=$model->root()->findAll();
                $this->buildTree($records);  //---V.Shirokov
	
		$this->render('index', array(
			'records' => $this->recset,  //---V.Shirokov
			'model' => $model,
			'modelBehaviors' => $modelBehaviors,
		));
		}
                else {
		$this->render('index', array(
			'records' => $records,
			'model' => $model,
			'modelBehaviors' => $modelBehaviors,
		));
                }
	}

       
	public function actionUpdate($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		$record = $model->findByPk($id);
		
		if (isset($_POST[$this->model])) {
			$record->attributes = $_POST[$this->model];
			
			if ($record->validate()){
				$record->save();
				$this->redirect($this->createUrl('index', $this->actionParams));
			}
		}
		
		$this->render('update', array(
			'record' => $record,
			'model' => $model,
			'modelBehaviors' => $modelBehaviors,
		));
	}
	
	public function actionAdd($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		$record = $model;
		
		if (isset($_POST[$this->model])) {
			$record->attributes = $_POST[$this->model];
			
			if (isset($modelBehaviors['tree']) && $parent) {
				$record->parent_id = $parent;
			}
			
			if (!empty($master)) {
				$record->{$this->section->foreignColumn} = $master;
			}
			
			if ($record->validate()){
				$record->save();
				$this->redirect($this->createUrl('index', $this->actionParams));
			}
		}
		
		
		
		$this->render('add', array(
			'record' => $record,
			'model' => $model,
			'modelBehaviors' => $modelBehaviors,
		));
	}
	
	public function actionDelete($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		if (isset($modelBehaviors['deleted']) && $id !== NULL){
			$model->findByPk($id)->remove()->save();
		} elseif ($id != NULL){
			$model->deleteByPk($id);
		}
		$this->redirect($this->createUrl('index', $this->actionParams));
	}
	
	public function actionActivate($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		if (isset($modelBehaviors['active']) && $id !== NULL){
			$model->findByPk($id)->activate()->save();
		}
		
		$this->redirect($this->createUrl('index', $this->actionParams));
	}
	
	public function actionDeactivate($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		if (isset($modelBehaviors['active']) && $id !== NULL){
			$model->findByPk($id)->deActivate()->save();
		}
		
		$this->redirect($this->createUrl('index', $this->actionParams));
	}
	
	/**
	 * Возвращает текущую модель
	 */
	public function getModel()
	{
		if ($this->_model == NULL)
		{
				$this->_model = $this->section->model;	
		}
		
		return $this->_model;
	}

	public function getBreadcrumbs()
	{
		if ($this->_breadcrumbs === NULL) {
			$this->_breadcrumbs = $this->section->getBreadcrumbs($this->actionParams);	
		}
		
		return $this->_breadcrumbs;
	}
}