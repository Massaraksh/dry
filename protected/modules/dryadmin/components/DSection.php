<?php
class DSection extends CComponent
{
	static $structure;
	
	public $controller;
	
	public $sectionId;
	
	public $label;
	
	public $titleColumn;
	
	public $model;
	
	public $masterParams;
	
	public $masterRelation;
	
	protected $_master;
	
	protected $_children;
		
	protected $_foreignColumn;
	
	protected $_breadcrumbs;
	
	public function __construct($sectionId, $controller = NULL)
	{
		$structure = Yii::app()->getModule('dryadmin')->structure;
		self::$structure = $structure;
		$this->sectionId = $sectionId;
		
		$this->controller = $controller;
		$this->label = self::$structure[$sectionId]['label'];
		$this->titleColumn = self::$structure[$sectionId]['title_column'];
		$this->model = self::$structure[$sectionId]['model'];
		$this->masterParams = self::$structure[$sectionId]['master'];
		$this->masterRelation = self::$structure[$sectionId]['master']['relation'];
	}
	
	public function getMaster()
	{
		if ($this->_master === NULL && $this->masterParams != NULL) {
			$this->_master = new DSection($this->masterParams['sectionId']); 
		}
		return $this->_master;
	}

	
	
	public function getChildren()
	{
		if ($this->_children === NULL) {
			$this->_children = array();
			
			foreach (self::$structure as $sectionId => $section)
			{
				if (isset($section['master']) && $section['master']['sectionId'] === $this->sectionId) {
					$this->_children[$sectionId] = new DSection($sectionId);
				}
			}
		}
		
		return $this->_children;
	}
	
	public function getForeignColumn()
	{
		if ($this->_foreignColumn === NULL && $this->master != NULL) {
			$masterModel = new $this->master->model();
			$masterRelations = $masterModel->relations();
			$this->_foreignColumn = $masterRelations[$this->masterRelation][2];
		}
		
		return $this->_foreignColumn;
	}

	public function getBreadcrumbs($params)
	{
		if ($this->_breadcrumbs === NULL) {
			$model = new $this->model();
			$modelBehaviors = $model->behaviors();
			
			
			$this->_breadcrumbs = array();
			
			// master-модель
			if ($this->master && $params['master'])
			{
				$masterParams = array('id' => $params['master'], 'section' => $this->masterParams['sectionId']);
				$this->_breadcrumbs += ($this->master->getBreadcrumbs($masterParams));
			}
			
			// Название текущего раздела
			if ($params['master'])
				$this->_breadcrumbs[$this->label] = Yii::app()->controller->createUrl('index', array('section' => $this->sectionId, 'master' => $params['master']));
			else
				$this->_breadcrumbs[$this->label] = Yii::app()->controller->createUrl('index', array('section' => $this->sectionId));
			
			// Цепочка родителей
			$this->_breadcrumbs += $this->treeBreadcrumbs($params);
			
			// Текущая запись
			if ($this->controller) {
				if ($this->controller->action->id == 'add')
					$label = 'Добавить';
				elseif ($this->controller->action->id == 'update')
				{
					if ($params['id'])
						$record = $model->findByPk($params['id']);
					$label = $record->{$this->titleColumn}.' [редактировать]';
				}
			}
			elseif ($params['id']) {
				$record = $model->findByPk($params['id']);
				$label = $record->{$this->titleColumn};
			}
			
			if ($label) $this->_breadcrumbs[$label] = NULL;
			
		}
		
		return $this->_breadcrumbs;
	}
	
	private function treeBreadcrumbs($params)
	{
		$breadcrumbs = array();
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		if (isset($modelBehaviors['tree'])) {
			
			if (!isset($params['parent']) && isset($params['id'])) {
				$params['parent'] = $model->findByPk($params['id'])->parent->primaryKey;
			}
			
			if (!$params['parent']) return $breadcrumbs;
			
			$parent = $model->findByPk($params['parent']);

			// Если мы в редактировании записи, то прописываем ссылку на первого родителя
			$breadcrumbs[$parent->{$this->titleColumn}] = ($params['id']) ? array('index', 'section' => $params['section'], 'parent' => $parent->primaryKey) : NULL;
			
			// Собираем остальных родителей
			if ($parent->parents)
			{
				foreach ($parent->parents as $p)
				{
					$breadcrumbs[$p->{$this->titleColumn}] = array('index', 'section' => $params['section'], 'parent' => $p->primaryKey);
				}
			}
			
		}
		
		return array_reverse($breadcrumbs);
	}

}
