<?php echo CHtml::beginForm('', 'post', array('class' => 'form-stacked')) ?>
	
	<div class="clearfix <?php if (isset($model->errors['login'])) echo 'error';?>">
		<?php echo CHtml::activeLabel($model, 'login'); ?>
		<div class="input">
			<?php echo CHtml::activeTextField($model, 'login', array('class' => 'xlarge')); ?>
			<span class="help-inline"><?php echo $model->getError('login');?></span>
		</div>
	</div>
	
	<div class="clearfix <?php if (isset($model->errors['password'])) echo 'error';?>">
		<?php echo CHtml::activeLabel($model, 'password'); ?>
		<div class="input">
			<?php echo CHtml::activePasswordField($model, 'password', array('class' => 'xlarge')); ?>
			<span class="help-inline"><?php echo $model->getError('password');?></span>
		</div>
	</div>
	
	<div class="actions">
		<?php echo CHtml::submitButton('Войти'); ?>
	</div>
	
	
<?php echo CHtml::endForm(); ?>
