<?php if (isset($records)): ?>
	<table class="zebra-striped">
		<thead>
			<tr>
				<th>#</th>
				<?php $attributeLabels = $model->attributeLabels(); ?>
				<?php foreach ($model->fields() as $attribute => $field): ?>
					<?php if (in_array('table', $field) && key_exists('type', $field)):?>
						<th><?php echo $attributeLabels[$attribute]; ?></th>
					<?php endif;?>
				<?php endforeach;?>
				<th colspan="2"></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($records as $record): ?>
				<tr>
					<td>
						<?php echo $record->primaryKey; ?>
						<?php if (isset($modelBehaviors['tree'])): ?>
							<a href="<?php echo $this->createUrl('add', array_merge($this->actionParams, array('parent' => $record->primaryKey)));?>" title="Добавить в ветку">+</a>

						<?php endif;?>
					</td>
					
					<?php $step=0;foreach ($model->fields() as $attribute => $field): ?>
						<?php if (in_array('table', $field) && key_exists('type', $field)):?>
							<td>
    <?php if($step==0)for($i=0;$i<$record->level;$i++)if($i==$record->level-1) echo "<img src='/images/level.png'>"; 
    else echo "<img src='/images/empty.png'>";$step++;echo $record->$attribute; ?>
                                                        </td>
						<?php endif;?>
					<?php endforeach;?>
					
					<td>
						<?php foreach ($this->section->children as $sectionId => $section): ?>
							<a href="<?php echo $this->createUrl('index', array('master' => $record->primaryKey, 'section' => $sectionId)); ?>"><?php echo $section->label; ?> (<?php echo count($record->{$section->masterRelation}); ?>)</a>
						<?php endforeach;?>
					</td>
					
					<td class="record-actions">
						<a href="<?php echo $this->createUrl('update', array_merge($this->actionParams, array('id'=>$record->primaryKey))); ?>" class="iconic pencil" title="Редактировать"></a>
						<a href="<?php echo $this->createUrl('delete', array_merge($this->actionParams, array('id'=>$record->primaryKey))); ?>" class="iconic x hover-red" title="Удалить"></a>
						
						<?php if (isset($modelBehaviors['active'])):?>
							<?php if ($record->isActive):?>
								<a href="<?php echo $this->createUrl('deactivate', array_merge($this->actionParams, array('id'=>$record->primaryKey))); ?>" class="iconic check-alt green" title="Запись активна. Нажмите чтобы деактивировать"></a>
							<?php else:?>
								<a href="<?php echo $this->createUrl('activate',  array_merge($this->actionParams, array('id'=>$record->primaryKey))); ?>" class="iconic check-alt hover-green" title="Запись неактивна. Нажмите чтобы активировать"></a>
							<?php endif; ?>
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach;?>
		</tbody>	
	</table>
	
	<a class="btn primary" href="<?php echo $this->createUrl('add', $this->actionParams);?>">Добавить</a>
	
<?php endif;?>