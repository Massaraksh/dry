<div id="finder"></div>

<script type="text/javascript">
$(document).ready(function() {
	 
	var funcNum = window.location.search.replace(/^.*CKEditorFuncNum=(\d+).*$/, "$1");
	var langCode = window.location.search.replace(/^.*langCode=([a-z]{2}).*$/, "$1");
	
	$('#finder').elfinder({
		url : '<?php echo $this->module->assetsUrl?>/elfinder/connectors/php/connector.php',
		lang : langCode,
		editorCallback : function(url) {
			window.opener.CKEDITOR.tools.callFunction(funcNum, url);
			window.close();
    	}
	});
})
</script>
