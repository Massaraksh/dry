<?php
class LoginForm extends CFormModel
{
	public $login;
	public $password;
	
	/**
	 * @var DUserIdentity
	 */
	private $_identity;
	
	public function rules()
	{
		return array(
			array('login, password', 'required'),
			array('password', 'authenticate'),
		);
	}
	
	public function authenticate($attribute, $params)
	{
		$this->_identity = new DUserIdentity($this->login, $this->password);
		if (!$this->_identity->authenticate()) {
			$this->addError('password', 'Неправильный логин или пароль.');
		}
	}
	
	public function getIdentity()
	{
		return $this->_identity;
	}
}