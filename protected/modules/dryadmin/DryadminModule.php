<?php

class DryadminModule extends CWebModule
{
	protected $_assetsUrl;
	
	
	protected $_structure;
	
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('dryadmin.assets'));
        return $this->_assetsUrl;
	}
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		
		// import the module-level models and components
		$this->setImport(array(
			'dryadmin.models.*',
			'dryadmin.models.form.*',
			'dryadmin.components.*',
			'dryadmin.widgets.*',
			'zii.widgets.*',
		));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// Проверка авторизации
			if (Yii::app()->user->isGuest && Yii::app()->controller->id != 'login')
				Yii::app()->user->loginRequired();
			
			return true;
		}
		else
			return false;
	}
	
	public function getStructure()
	{
		return array(
			'page' => array(
				'label' => 'Страницы',
				'model' => 'Page',
				'title_column' => 'title',
			),
			'article' => array(
				'label' => 'Статьи',
				'model' => 'Article',
				'title_column' => 'title',
			),
			'catalog' => array(
				'label' => 'Каталог',
				'model' => 'Catalog',
				'title_column' => 'title',
			),
			'item' => array(
				'master' => array('sectionId' => 'catalog', 'relation' => 'items'), // ключ массива хозяина
				'label' => 'Позиции',
				'model' => 'Item',
				'title_column' => 'title',
			),
		);
	}
	
}
