<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'page-PageForm-form',
		'enableAjaxValidation'=>false,
	)); ?>
	<?php echo $form->errorSummary($this->record);?>
	<fieldset>
		<?php foreach ($this->record->fields() as $attribute => $field): ?>
			<?php $this->widget('DField', array(
				'form' => $form,
				'attribute' => $attribute,
				'field' => $field,
				'record' => $this->record,
			)); ?>
		<?php endforeach;?>
	</fieldset>
	<div class="actions span12">
		<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn primary')); ?>
		<a class="btn" href="<?php echo $this->controller->createUrl('index', $this->controller->actionParams); ?>">Отмена</a>
	</div>
	
<?php $this->endWidget(); ?>

<script type="text/javascript">
$(document).ready(function (){
	$( '.html-editor' ).ckeditor({toolbar: 'Standard', filebrowserBrowseUrl: '<?php echo $this->controller->createUrl('filemanager/index'); ?>'});
});
</script>