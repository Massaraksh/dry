<?php
class PageTest extends CDbTestCase
{
	public $fixtures = array(
		'pages' => 'Page',
	);
	
	public function testChildren()
	{
		$page = Page::model()->findByPk(3);
		$this->assertEquals(3, count($page->children));
		
		$page = Page::model()->findByPk(8);
		$this->assertEquals(0, count($page->children));
	}
	
	public function testParent()
	{
		$page = Page::model()->findByPk(5);
		$this->assertEquals(3, $page->parent->primaryKey);
		
		$page = Page::model()->findByPk(8);
		$this->assertEquals(NULL, $page->parent);
		
		$page = Page::model()->findByPk(1);
		$this->assertEquals(NULL, $page->parent);
	}
	
	public function testFullUrl()
	{
		$page = Page::model()->findByPk(1);
		$this->assertEquals('/', $page->fullUrl);
		
		$page = Page::model()->findByPk(8);
		$this->assertEquals('/articles', $page->fullUrl);
		
		$page = Page::model()->findByPk(5);
		$this->assertEquals('/about/personal', $page->fullUrl);
	}
}