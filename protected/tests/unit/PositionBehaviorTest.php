<?php
class PositionBehaviorTest extends CDbTestCase
{
	public $fixtures = array(
		'pages' => 'Page'
	);
	
	function testSorted(){
		$p = new Page();
		$pages = $p->sorted()->findAll();
		
		// Убеждаемся что позиции идут в возврастающем порядке
		$position = 0;
		foreach ($pages as $page) {
			$this->assertTrue($position <= $page->position);
			$position = $page->position;
		}
	}	
	
}