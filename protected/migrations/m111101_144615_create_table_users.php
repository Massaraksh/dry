<?php

class m111101_144615_create_table_users extends CDbMigration
{
	public function up()
	{
		$this->createTable('users', array(
			'id' => 'pk',
			'name' => 'text',
			'email' => 'text',
			'login' => 'text',
			'password' => 'text',
			'active' => 'boolean NOT NULL DEFAULT 1',
			'deleted' => 'boolean NOT NULL DEFAULT 0',
			'created' => 'timestamp DEFAULT CURRENT_TIMESTAMP'
		));
	}

	public function down()
	{
		$this->dropTable('users');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
