<?php

class m111101_143928_create_table_pages extends CDbMigration
{
	public function up()
	{
		$this->createTable('pages', array(
			'id' => 'pk',
			'parent_id' => 'integer NOT NULL DEFAULT 0',
			'title' => 'text',
			'body' => 'text',
			'url' => 'text',
			'controller' => 'text',
			'deleted' => 'boolean NOT NULL DEFAULT 0',
			'active' => 'boolean NOT NULL DEFAULT 1',
			'created' => 'timestamp DEFAULT CURRENT_TIMESTAMP'
		));
	}

	public function down()
	{
		$this->dropTable('pages');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
