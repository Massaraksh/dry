<?php

class m111101_145126_create_table_catalog extends CDbMigration
{
	public function up()
	{
		$this->createTable('catalog', array(
			'id' => 'pk',
			'parent_id' => 'integer NOT NULL DEFAULT 0',
			'title' => 'text',
			'active' => 'boolean NOT NULL DEFAULT 1',
			'deleted' => 'boolean NOT NULL DEFAULT 0',
			'created' => 'timestamp DEFAULT CURRENT_TIMESTAMP',
		));
	}

	public function down()
	{
		$this->dropTable('catalog');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
